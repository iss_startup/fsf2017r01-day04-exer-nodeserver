// TODO: 1. Import or load modules used by this application


// TODO: 2. Define NODE_PORT


// Create server
var server = http.createServer(handleRequest);

// Define handleRequest function
// handleRequest handles requests and sends responses
function handleRequest(request, response){

    // Just printing some spaces
    console.log("\n\n");

    // so this is how Node.js's http module handles requests for specific paths
    // request.url just reads the client's target url, which happens to be stored in the request object
    // we'll learn more about request objects later
    switch(request.url){
        case '/syncread' :
            console.log("============= You've requested for /syncread ==========");
            logFilesSync();
            console.log(">>> Just left logFilesSync()");
            break;
        case '/asyncread' :
            console.log("============= You've requested for /asyncread ==========");
            logFiles();
            console.log(">>> Just left logFiles()");
            break;
        default: // do nothin but console out
            console.log("============= You've requested for %s ==========", request.url);
            break;
    }

    console.log("============= Sending response to client. Bye for now. ==========");
    response.end('It Works!! Path Hit: ' + request.url);
} // END handleRequest()


// Function to log files synchronously
function logFilesSync() {
    var  filenames,
         i;

    // fs supports async and sync versions of its methods.
    // readdirSync is the sync version of a method that reads a directory.
    filenames = fs.readdirSync(".");
    for (i = 0; i < filenames.length; i++) {
        console.log("File %s is %s: ", i, filenames[i]);
    }
    console.log("Process ID is %s", process.pid);
} // END logFilesSync()


// Function to log files asynchronously
function logFiles() {
    var  i;

    // fs supports async and sync versions of its methods.
    // readdir is the async version of a method that reads a directory.
    // TODO: 3. Use fs.readdir to asynchronously read and list the files in the current directory
    // TODO: 3.1 Go to https://nodejs.org/api/fs.html to learn how to use fs.readdir.
    // TODO: 3.2 Replace PATH with actual path you want to read
    // TODO: 3.3 Replace CB with your own callback function
    fs.readdir(PATH, CB);

    console.log("Process ID is %s", process.pid);
} // END logFiles()

// Start server
server.listen(NODE_PORT, function(){
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", NODE_PORT);
});